# Project makefile for autodafe
#
# Requires Python 3 and asciidoctor. Tesrts also want pylint
#
# makemake.svg is from https://en.wikipedia.org/wiki/Makemake#/media/File:Makemake_symbol_(bold).svg
# Required attribution: By PlanetUser - Own work, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=45820499

VERSION=$(shell sed -n <NEWS.adoc '/::/s/^\([0-9][^:]*\).*/\1/p' | head -1)

SOURCES = README.adoc COPYING NEWS.adoc de-autoconfiscation.adoc makemake makemake.adoc makemake.1 Makefile \
	control TODO.adoc

.SUFFIXES: .html .adoc .1

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

clean:
	rm -f ascii ascii.o splashscreen.h nametable.h
	rm -f *.rpm *.tar.gz MANIFEST *.1 *.html

prefix?=/usr/local
mandir?=share/man
target=$(DESTDIR)$(prefix)

install: makemake.1
	install -d "$(target)/bin"
	install -d "$(target)/$(mandir)/man1"
	install -m 755 makemake "$(target)/bin"
	install -m 644 makemake.1 "$(target)/$(mandir)/man1"
	if [ -O makemake.1 ]; then rm makemake.1; fi

uninstall:
	-rm "$(target)/$(mandir)/man1/makemake.1"
	-rm "$(target)/bin/makemake"
	-rmdir -p "$(target)/$(mandir)/man1"
	-rmdir -p "$(target)/bin"

reflow:
	@black -q makemake

pylint:
	@pylint --score=n makemake

check: pylint
	cd tests; $(MAKE) --quiet

# Don't do pylint on Gitlab, there's a version-skew problem.
cicheck:
	cd tests; $(MAKE) --quiet

version:
	@echo $(VERSION)

autodafe-$(VERSION).tar.gz: $(SOURCES)
	@ls $(SOURCES) | sed s:^:autodafe-$(VERSION)/: >MANIFEST
	@(cd ..; ln -s autodafe autodafe-$(VERSION))
	(cd ..; tar -czf autodafe/autodafe-$(VERSION).tar.gz `cat autodafe/MANIFEST`)
	@ls -l autodafe-$(VERSION).tar.gz
	@(cd ..; rm autodafe-$(VERSION))

dist: autodafe-$(VERSION).tar.gz

release: autodafe-$(VERSION).tar.gz makemake.html de-autoconfiscation.html
	shipper version=$(VERSION) | sh -e -x

refresh: makemake.html
	shipper -N -w version=$(VERSION) | sh -e -x
